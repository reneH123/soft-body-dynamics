#ifndef BOX_CPP_
#define BOX_CPP_

#include "geometry.hpp"
#include <string.h>
#include "include.hpp"

Box::Box() {
	reSize(3, 2, 5);
	memcpy(&vertices, cubeVertices, sizeof(vertices));
	for (int i = 0; i < maxVertices; i++) {
		vertices[i] = vertices[i].ElementwiseMultiply(size);
	}
}

void Box::Paint() {
	for (int i = 0; i < NUMCUBEEDGES; i++) {
		glColor3f(0.7, 0.9, 0.2);
		glBegin(GL_POLYGON);
		glVertex3dv(vertices[cubeEdges[i][0]].array);
		glVertex3dv(vertices[cubeEdges[i][1]].array);
		glVertex3dv(vertices[cubeEdges[i][2]].array);
		glVertex3dv(vertices[cubeEdges[i][3]].array);
		glEnd();
	}
}

void Box::visit(Cloth *cloth) {
	ClothParticle *p;
	int x, y;

	for (y = 0; y < cloth->m_ResolutionY; y++)
		for (x = 0; x < cloth->m_ResolutionX; x++) {
			p = cloth->m_Particles[x + y * cloth->m_ResolutionX];
			p->fixed = false;
			if ((p->position.y < this->size.y + 0.04)
					&& (abs(p->position.x) < this->size.x)
					&& (abs(p->position.z) < this->size.z)) {
				p->force += p->position;
				p->velocity += p->position;
			}
		}
}

#endif
