#ifndef CLOTH_HPP_
#define CLOTH_HPP_

class Collision;
class Ground;

#include <stdlib.h>
#include "clothparticle.hpp"
#include "geometry.hpp"
#include <GL/glu.h>

typedef struct {
	int nw, no, sw, so;
} Facette;

class Cloth {
private:
	bool normals_Activated; // TODO: refactor all following parameters!
	bool Euler_Activated;
	bool RungeKutta_Activated;
	bool MidPoint_Activated;

	Vector3D m_ClothStartPosition, m_ClothEndPosition;
	double m_ClothMass;
	double m_StructuralStiffness;
	double m_ShearStiffness;
	double m_FlexionStiffness;
	double m_Damping;
	double m_Fluid;// ..=0.014;

	ClothSpring **m_Springs;
	ClothSpring* findSpring(ClothParticle* from, ClothParticle* to, ClothSpring::SpringTypes type);

	Facette *faces;
	GLuint texture[1];

	Vector3D det_f(int i, int j, int k, int l, ClothSpring::SpringTypes type);
	// position vector of mass at (x,y)
	Vector3D Pos(int x, int y);
	// normal vector for mass at (x,y)
	Vector3D getNormal(int x, int y);
	// this method creates a simple rectangular cloth patch
	// by connecting the particle grid with 3 spring types
	void InitializeClothStructure();
	// here all internal forces have to be calculated
	void CalculateInternalForce(double delta_t);
	// here all forces, e.g. gravity, damping, wind and for viscous fluids
	void CalculateExternalForces(double delta_t);

	void SolveImplicitEuler(double delta_t);
	void MidPointMethod(double delta_t);
	void SolveRungeKutta(double delta_t);

	void InitializeFaces();

	Collision *collisionProxy;
	Ground* ground;   // TODO: refactor to DECORATOR !

	void testCollision(Collision *cp);

	void LoadGLTexture();
public:
	void InvertNormalsActivated();
	void ChooseIntegrator(int solver);
	int m_ResolutionX, m_ResolutionY;
	ClothParticle **m_Particles;
	Vector3D **m_old_Positions;
	double m_Friction;
	void setCollision(Collision *collisionProxy);
	Cloth();
	~Cloth();
	void Initialize();
	void ReInitialize();
	void Paint();
	void Step(double delta_t);

	void IncMass(double dmass); // TODO: use DECORATOR/ITERATOR instead!
	void IncSuspension(double dstructural);
	void IncDamping(double ddamping);
	void IncFluid(double dfluid);
	void DecFluid(double dfluid);
	void IncFriction(double dfriction);
	void DecFriction(double dfriction);
};

#endif
