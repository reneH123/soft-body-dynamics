Author: René Haberland, 28th Aug 2016, 2020

This software is licenced under License Creative Commons, CC BY-NC  https://creativecommons.org/licenses/by-nd/4.0/, the license conditions are enclosed.


Use the BUILDER-script (see BUILDER-project) for a simple project-build. Change to the top-level directory (where the builder.conf is located and run builder inside that directory).

Once successfully build, __BUILDER/main will be the executable file.


This project requires OpenGL and GLU and GLUT to be installed and included/linked as specified in the BUILDER-script (g++ is specified by default).


The simulation is based on Xavier Provot's mass-spring model for soft body dynamics. The main ideas are reference implemented as provided in his paper:

@inproceedings{DBLP:conf/egcas/Provot97,
  author    = {Xavier Provot},
  editor    = {Daniel Thalmann and
               Michiel van de Panne},
  title     = {Collision and self-collision handling in cloth model dedicated to
               design garments},
  booktitle = {Proceedings of the Eurographics Workshop on Computer Animation and
               Simulation 1997, Budapest, Hungary, September 2-3, 1997},
  series    = {Eurographics},
  pages     = {177--189},
  publisher = {Springer},
  year      = {1997},
  url       = {https://doi.org/10.1007/978-3-7091-6874-5\_13},
  doi       = {10.1007/978-3-7091-6874-5\_13},
  timestamp = {Mon, 11 May 2020 15:03:26 +0200},
  biburl    = {https://dblp.org/rec/conf/egcas/Provot97.bib},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}

More information on body dynamics can be found here:

https://en.wikipedia.org/wiki/Rigid_body_dynamics
https://en.wikipedia.org/wiki/Soft-body_dynamics


